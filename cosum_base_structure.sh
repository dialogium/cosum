#!/bin/bash

set -e

php artisan make:model Profile -m
php artisan make:controller ProfileController

php artisan make:model Gegenstand -m
php artisan make:controller GegenstaendeController --resource

php artisan make:model Ort -m
php artisan make:controller OrteController --resource

php artisan make:model Sichtbarkeit -m
php artisan make:controller SichtbarkeitenController --resource
php artisan make:model Kontakt -m
php artisan make:controller KontakteController --resource
php artisan make:model SubjGruppe -m
php artisan make:controller SubjGruppenController --resource

mkdir -p resources/views/{profile,gegenstaende,orte,sichtbarkeiten,subjgruppen}
touch resources/views/{kontakte,profile,gegenstaende,orte,sichtbarkeiten,subjgruppen}/{index,show,create,edit}.blade.php

