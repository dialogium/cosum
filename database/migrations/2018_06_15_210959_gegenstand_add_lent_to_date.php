<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GegenstandAddLentToDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gegenstands', function (Blueprint $table) {
            $table->timestamp('lent_to_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gegenstands', function (Blueprint $table) {
            $table->dropColumn('lent_to_date');
        });
    }
}
