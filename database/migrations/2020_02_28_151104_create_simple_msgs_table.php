<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSimpleMsgsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('simple_msgs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_id');
            $table->integer('to_user_id');
            $table->integer('from_user_id');
            $table->mediumText('text')->nullable();
            $table->integer('signal')->nullable();
            $table->mediumText('reply')->nullable();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('simple_msgs');
    }
}
