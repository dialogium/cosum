<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDesiredDatesToSimplmsg extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('simple_msgs', function (Blueprint $table) {
            $table->timestamp('desired_date_from')->nullable();
            $table->timestamp('desired_date_to')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('simple_msgs', function (Blueprint $table) {
            $table->dropColumn('desired_date_from');
            $table->dropColumn('desired_date_to');
        });
    }
}
