<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNameUserIdBeschreibung extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orts', function (Blueprint $table) {
            if (!Schema::hasColumn('orts','name')) {
                $table->string('name');
            }
            if (!Schema::hasColumn('orts','beschreibung')) {
                $table->mediumText('beschreibung');
            }
            if (!Schema::hasColumn('orts','user_id')) {
                $table->integer('user_id');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orts', function (Blueprint $table) {
            if (Schema::hasColumn('orts','name')) {
                $table->dropColumn('name');
            }
            if (Schema::hasColumn('orts','beschreibung')) {
                $table->dropColumn('beschreibung');
            }
            if (Schema::hasColumn('orts','user_id')) {
                $table->dropColumn('user_id');
            }
        });
    }
}
