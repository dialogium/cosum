<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GegenstaendeAddHash extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gegenstands', function (Blueprint $table) {
            $table->string('hash')->nullable();
            $table->index('hash');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gegenstaende', function (Blueprint $table) {
            $table->dropColumn('hash');
            $table->dropIndex(['hash']);
        });
    }
}
