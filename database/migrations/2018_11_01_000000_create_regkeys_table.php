<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegkeysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('regkeys', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('creator_id')->unsigned();//operator
            $table->integer('user_id')->unsigned()->nullable(); //registering user
            $table->timestamp('active');
            $table->string('keyname');
            $table->string('keycode');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('regkeys');
    }
}
