<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Lendingprocess extends Model
{
    public function user(){
        return $this->belongsTo('App\User');
    }

    public static function top10(){
        $ids= DB::table('lendingprocesses')
                ->select('gegenstand_id', DB::raw('COUNT(gegenstand_id) AS occurrence'))
                ->groupBy('gegenstand_id')
                ->orderBy('occurrence', 'DESC')
                ->limit(10)
                ->get();

        $gegenstaende = [];
        foreach($ids as $pair){
             $gegenstand= Gegenstand::find($pair->gegenstand_id);
             if ($gegenstand){
                $gegenstand->occurrence= $pair->occurrence; //with occurrence
                array_push($gegenstaende, $gegenstand);
             }
        }
        return $gegenstaende;
    }
}
