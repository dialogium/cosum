<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SimpleMsg extends Model
{
    // Table Name
    protected $table = 'simple_msgs';
    // Primary Key
    public $primaryKey = 'id';
    // Timestamps
    public $timestamps = true;

/*          $table->increments('id');
            $table->integer('item_id');
            $table->integer('to_user_id');
            $table->integer('from_user_id')->nullable();
            $table->mediumText('text');
            $table->integer('signal');
            $table->mediumText('reply');
            $table->timestamps();
 */
}
