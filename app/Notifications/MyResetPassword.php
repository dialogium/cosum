<?php

namespace App\Notifications;

use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Notifications\Messages\MailMessage;

class MyResetPassword extends ResetPassword
{
    /**
     * The password reset token.
     *
     * @var string
     */
    public $token;

    /**
     * Create a notification instance.
     *
     * @param  string  $token
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }
    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->markdown('notifications.pwreset_mail')
            ->subject('Cosum: Passwort zurücksetzen')
            ->greeting('Hallo!')
            ->line('Soll das Passwort für den Benutzer **'.$notifiable->name.'** zurückgesetzt werden?')
            ->action('Passwort zurücksetzen', url(config('app.url').route('password.reset', $this->token, false)))
            ->line('Sollte die Anfrage zur Zurücksetzung verkehrt sein, kann diese Email ignoriert werden.')
            ->salutation("Mit besten Grüßen, Cosum");
    }
}
