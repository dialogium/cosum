<?php

namespace App\Notifications;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Lang;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\HtmlString;
use App\Contact;
use App\Gegenstand;
use App\User;

class LendingExpiryEmail extends Notification
{
    public $url="";
    public $sender="jemand";
    public $msg_body="";
    public $expiryReminder = null;

    public function __construct($url,$sender, $msg_body, $expiryReminder)
    {
        $this->url= $url;
        $this->msg_body= $msg_body;
        $this->sender= $sender;
        $this->expiryReminder = $expiryReminder;
    }
    /**
     * The callback that should be used to build the mail message.
     *
     * @var \Closure|null
     */
    public static $toMailCallback;

    /**
     * Get the notification's channels.
     *
     * @param  mixed  $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    public function url(){
        return $this->url;
    }

    public function owner(){
        $owner= User::find($this->item()->user_id);
        return $owner->name;
    }

    public function item(){
       return Gegenstand::find($this->expiryReminder->item_id);
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if (static::$toMailCallback) {
            return call_user_func(static::$toMailCallback, $notifiable);
        }

        return (new MailMessage)
            ->subject('Rückgabe-Erinnerung')
            ->greeting('Hallo!')
            ->line( new HtmlString ("<h2>". "Rückgabe-Erinnerung" ."</h2>" ))
            ->line( 'Ein ausgeliehener Gegenstand erreicht seine Leihfrist.')
            ->line( 'Diese Mail soll daran erinnern, dass sich der Eigentümer über die Rückgabe des Gegenstandes freuen wird.')
            ->line( 'Hier ein paar Details:')
            ->line( new HtmlString ("Gegenstand: "."<b>". $this->item()->name ."</b>" ))
            ->line( 'Eigentümer: '.$this->owner())
            ->line( 'Ablaufdatum: '. Carbon::parse($this->expiryReminder->expiry_date)->toDateString() )

            ->action(
                'Hier zum Gegenstand',
                $this->url()
            )
            ->line( new HtmlString ("<hr/>" ))
            ->line('Gar nicht bei COSUM registriert? Dann kann diese Email ignoriert werden.')
            ->salutation('Mit besten Grüßen, Cosum');
    }

    /**
     * Set a callback that should be used when building the notification mail message.
     *
     * @param  \Closure  $callback
     * @return void
     */
    public static function toMailUsing($callback)
    {
        static::$toMailCallback = $callback;
    }
}
