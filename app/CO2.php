<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Lendingprocess;
use Carbon\Carbon;

class CO2 extends Model
{
    public const CO2_FACTOR=1;
    public $days=0;
    public $total_savings=0;
        
    protected function lendingdays( $lendingprocesses ){
        $this->days=0;
        foreach ($lendingprocesses as $proc){
            $this->days+= $this->lending_diff_days($proc);
        }
        
        return $this->days;
    }

    protected function lending_diff_days( $proc ){
        if (!$proc->finished_at || !$proc->started_at )
            return 0;

        $c_f_at=Carbon::parse($proc->finished_at);
        $c_s_at=Carbon::parse($proc->started_at);
        $diff= $c_f_at->diffInDays($c_s_at);
        return $diff;
    }

    /**
     * @param array of App\Lendingprocess $lprocs 
     * @return total of co2 saving for the lendingprocesses provided
     */
    public function calc( $procs ){
        $lending_days_total=$this->lendingdays($procs);
        $this->total_savings=0;
        $this->total_savings= CO2::CO2_FACTOR * $lending_days_total;
        return $this->total_savings;
    }

}
