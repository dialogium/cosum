<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Gegenstand;
use App\Contact;
use Illuminate\Support\Facades\URL;

class ExpiryReminder extends Model
{
    protected $fillable = [
        'sender_contact_id',
        'recepient_contact_id',
        'expiry_date',
        'item_id'
    ];

    public function isToday(){
	$res=(Carbon::parse($this->expiry_date)->toDateString() == Carbon::today()->toDateString());
	return $res;
    }

    public function wasShot(){
	return ($this->status =="shot");
    }

    public function isActive(){
	return ($this->status =="active");
    }

    public function fire(){
	if (Carbon::parse($this->expiry_date) == null){
		echo get_class($this).' ID:'. $this->id . " ERROR no date.\n";
		return ;
	}

	if (!$this->isActive()){
            //echo get_class($this).' ID:'. $this->id . " set for ". $this->expiry_date ." was shot"."\n";
		return ;
	}

	if (!$this->recepient_contact_id){
	    echo get_class($this).' ID:'. $this->id . " ERROR no recepient contact.\n";
	    $this->status="no recepient contact";
	    $this->save();
	    return ;
	}

        $isToday=$this->isToday();
        if ($isToday){
	    $c=Contact::find($this->recepient_contact_id);
	    $c_user= $c->getUser();
	    $gegenstand= Gegenstand::find($this->item_id);
	    $this->status="";
	    if ($c_user){
		    if ($c_user->profile()->first()->email_notifications_on){
		    	$c_user->sendLendingExpiryNotification(URL::to('/')."/gegenstaende/".$gegenstand->id, null, "", $this);
		    	$this->status="email sent.";
		    }else{
		    	$this->status="no email sent.";
		    }
            	    echo "[". Carbon::now() ."] ". get_class($this).' ID:'. $this->id . " set for ". $this->expiry_date ." fired "."\n";
	    }else{
            	    echo "[". Carbon::now() ."] ". get_class($this).' ID:'. $this->id . " set for ". $this->expiry_date ." no user to send mail to"."\n";
		    $this->status="no user to send mail to";
	    }

	    $this->save();
        }//else
           //echo get_class($this).' ID:'. $this->id . " set for ". $this->expiry_date ." not fired "."\n";
    }
}
