<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Token;
use App\Ort;
use App\Gegenstand;
use App\User;
use DB;

class OrteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['logs-out-banned-user','auth','verified','tos']);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $user_id = auth()->user()->id;
        $user = User::find($user_id);
        return view('orte.index')->with('orte', $user->orte);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $required_by= ($request->required_by)?? null;
        return view('orte.create')->with(['required_by' => $required_by ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	$this->validate($request, [
            'name' => 'required',
            'beschreibung' => 'required',
        ]);


        // Create Ort
        $ort = new Ort;
        $ort->name = $request->input('name');
        $ort->address= $request->input('address');
        $ort->beschreibung = $request->input('beschreibung');
        $ort->user_id = auth()->user()->id;
        $ort->hash= Token::Unique('orts', 'hash', 12);
        $ort->save();

        if ($request->has('required_by') && $request->input('required_by') == 'gegenstaende' ){
            return redirect('/gegenstaende/create')->with('success', 'Jetzt kann ein Gegenstand hinzugefügt werden.');
        }

        return redirect('/orte')->with('success', 'Ort angelegt.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ort = Ort::find($id);

        return view('orte.show')->with('ort', $ort);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ort = Ort::find($id);

        // Check for correct user
        if(auth()->user()->id !=$ort->user_id){
            return redirect('/orte')->with('error', 'Unauthorized Page');
        }

        return view('orte.edit')->with('ort', $ort);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
	$this->validate($request, [
            'name' => 'required',
            'beschreibung' => 'required',
            'location_logo_image' => 'image|nullable|max:999'
        ]);



	//update Ort
        $ort = Ort::find($id);
        $ort->name = $request->input('name');
        $ort->color = $request->input('color');
        $ort->address= $request->input('address');
        $ort->beschreibung = $request->input('beschreibung');

        // Handle File Upload
        if($request->hasFile('location_logo_image')){
            // Get filename with the extension
            $filenameWithExt = $request->file('location_logo_image')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('location_logo_image')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore= $filename.'_'.time().'.'.$extension;
            // Upload Image
            $path = $request->file('location_logo_image')->storeAs('public/location_images', $fileNameToStore);
            $ort->location_logo_image= $fileNameToStore;
        } else if (!$ort->location_logo_image) {
            $fileNameToStore = 'noimage.jpg';
            $ort->location_logo_image= $fileNameToStore;
        }

	$ort->save();
        return redirect('/orte')->with('success', 'Ort '. $ort->name .' aktualisiert.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {        
	$ort = Ort::find($id);

        // Check for correct user
        if(auth()->user()->id !=$ort->user_id){
            return redirect('/orte')->with('error', 'Unauthorized Page');
        }

        $gegenstaende = Gegenstand::where('ort_id',$ort->id)->get();
        if (count($gegenstaende) > 0 ){
            return back()->with('error', 'Es gehören noch Gegenstände zu diesem Ort.');
        }

        $ort->delete();
        return redirect('/orte')->with('success', 'Ort gelöscht');
    }
}
