<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Lendingprocess;
use Carbon\Carbon;
use App\CO2;
use App\Profile;
use App\Regkey;
use Illuminate\Support\Facades\Auth;
use App\Revelation;


class ProfileController extends Controller
{
     protected $co2;
     protected $redirectTo = '/profil';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['logs-out-banned-user','auth','verified','tos']);
        $this->co2= new CO2();
    }


    public function show(){
        $lendingprocesses=auth()->user()->lendingprocesses()->get();
        $multiplier=CO2::CO2_FACTOR;
        $this->co2->calc($lendingprocesses);

        return view('profile.show')->with(['lendingprocesses' => $lendingprocesses, 'lendingdays' => $this->co2->days, 'multiplier' => $multiplier , 'total_savings' => $this->co2->total_savings ]);
    }

    public function email_set(Request $request){
        $user=auth()->user();
        $user_id=$user->id;
        Log::notice('User '.$user_id.' requests email change from '.$request->input('old_email').' to '.$request->input('email'));
        $this->email_validator($request->all())->validate();

        $user->email= $request->input('email');
        $user->email_verified_at= null;
        $user->save();
        Log::notice('User '.$user_id.' successfully changed email from '.$request->input('old_email').' to '.$request->input('email'));
        $user->sendEmailVerificationNotification();

        return redirect('/profil')->with('success','Email geändert.');
    }

    public function update(Request $request){
        $user=auth()->user();
        $user_id=$user->id;
        $this->validator($request->all())->validate();
        $descr=$request->input('Beschreibungstext');
        $email_notifications_on=($request->input('email_notifications_on'))?1:0;
        $profile=$user->profile();
        $profile->update(['description' => $descr, 'email_notifications_on' => $email_notifications_on]);

        return redirect('/profil')->with('success','Profil aktualisiert.');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function email_validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|confirmed|string|email|max:255|unique:users',
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'Beschreibungstext' => 'string|max:400|nullable'
        ]);
    }

    protected function try_community(){
        $rks=auth()->user()->regkeys()->get();
        return view('profile.enter')->with(['communities' => $rks]);
    }

    public function community_description($id){
        $community=Regkey::find($id);
        $isInCommunity = auth()->user()->regkeys()->where('regkey_id',$id)->first();
        if (auth()->user()->isOperator() || $isInCommunity ){
            return view('regkeys.community_page')->with(['community' => $community]);
        }else
            abort(404);
    }

    protected function community_validator(array $data){
        return Validator::make($data, [
            'keyname' => [
                'required',
                Rule::exists('regkeys')->where(function ($query) use($data) {
                    $query->where('keyname', $data['keyname']);
                }),
            ],
            'keycode' => [
                'required',
                Rule::exists('regkeys')->where(function ($query) use($data) {
                    $query->where('keyname', $data['keyname'])->where('keycode',$data['keycode']);
                }),
            ]
        ]);
    }


    public function enter_community(Request $request){
        $user=auth()->user();
        $user_id=$user->id;
        $this->community_validator($request->all())->validate();

        $regkey=Regkey::where('keyname',$request->input('keyname'))->where('keycode',$request->input('keycode'))->first();

        if ($regkey->regusers()->where('user_id',$user->id)->count() <= 0){
            $regkey->regusers()->attach($user);

            $already="";
        }
        else
            $already=' schon zuvor';

        $user->auto_connect= $request->input('auto_connect');
        $user->save();

        if ($user->auto_connect){
            $regkey->auto_connect($user);
        }

        return redirect('/kontakt/gemeinschaft')->with('success','Gemeinschaft'.$already.' beigetreten.');
    }

    public function edit(){
        $profile=auth()->user()->profile;
        return view('profile.edit')->with(['profile' => $profile]);
    }

    public function confirm_delete_account(){
        return view('profile.delete');
    }

    public function softdelete(){
        $result= $this->user_overview();
        $user=auth()->user();
        //delete itemrequests
        $user->itemRequests()->delete();

        //delete his revelations from/to him
            //all for r->sender_id and r->rcpt_user_id  == user_id
            $revelations= Revelation::where('sender_id', auth()->user()->id)->get();
            foreach ($revelations as $revelation) {
                $revelation->forceDelete();
            }
        //delete contacts
            //owner_id
            //$user->contacts()->delete();
            $cts=$user->contacts()->get();
            foreach ($cts as $ct){
                $ct->disconnect();
                $ct->delete();
            }

            //delete items
            $user->gegenstaende()->delete();
        //delete orte
            //user_id
            $user->orte()->delete();
            //user_id
        //delete messages from/to him
            //from_user_id to_user_id
        Auth::logout();
        $user->delete(); //softdelete
        Log::notice("Löschungs-Notiz: user ".$user->id.",".$user->name .",".$user->email." ".json_encode($result));
        return redirect('/login')->with(['success' => 'Konto erfolgreich gelöscht.' ]);
    }

    private function user_overview(){
        $theuser=auth()->user();
        $regkeys=$theuser->regkeys()->get();
        $itemRequests=$theuser->itemRequests()->get();
        $contactcircles=$theuser->contactcircles()->get();
        $contacts=$theuser->contacts()->get();
        $orte=$theuser->orte()->get();
        $lendingprocesses=$theuser->lendingprocesses()->whereNull('finished_at')->whereNotNull('started_at')->get();
        $gegenstaende=$theuser->gegenstaende()->get();
        $revelations= Revelation::where('sender_id', auth()->user()->id)->get();

        return view('profile.summary')->with(['user' => $theuser,'regkeys' => $regkeys,'gegenstaende' => $gegenstaende,'orte' => $orte, 'anfragen' => $itemRequests, 'leihvorgaenge' => $lendingprocesses,'revelations' => $revelations, 'contactcircles' => $contactcircles, 'contacts' => $contacts]);
    }

    public function produce_log(){
        $result= $this->user_overview();
        $theuser=auth()->user();
        Log::notice("Überblicks-Notiz: user ".$theuser->id.",".$theuser->name .",".$theuser->email." ".json_encode($result));
        return $result;
    }

}
