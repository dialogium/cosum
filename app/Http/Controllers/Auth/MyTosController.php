<?php

namespace App\Http\Controllers\Auth;

use Dialogium\tos\Controllers\Auth\TosController;

class MyTosController extends TosController
{
    /*
    |--------------------------------------------------------------------------
    | Tos acception Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling presentation and agreement of Terms of Service (ToS) for any
    | user that recently registered with the application.
    |
    */

    /**
     * Where to redirect users after verification.
     *
     * @var string
     */
    protected $redirectTo = '/meine_gegenstaende';
}
