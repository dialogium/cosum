<?php

// resources/lang/de/contact.php

return [
    'public' => "öffentlich", 
    'communal' => "kommunal", 
    'private' => "privat",
    'hidden' => "versteckt" 
];
