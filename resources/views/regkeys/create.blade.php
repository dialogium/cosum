@extends ( View::exists('layouts.blueboard')?'layouts.blueboard' : 'blueboard::layouts.blueboard')

@section ('content')
    <a href="/regkeys" class="btn btn-sm btn-default btn-info front-btn">Zurück</a>
    <h1>Schlüssel hinzufügen</h1>
    {!! Form::open(['action' => 'RegkeysController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
        <div class="form-group">
            {{Form::label('keyname', 'Name/Gemeinschaft')}}
            {{Form::text('keyname', '', ['class' => 'form-control', 'placeholder' => 'Name'])}}
        </div>
        <div class="form-group">
            {{Form::label('keycode', 'Code')}}
            {{Form::text('keycode', '', ['class' => 'form-control', 'placeholder' => 'Code'])}}
        </div>
        <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12 top-buffer">
            {{Form::label('description', 'Beschreibung:')}}
            {{Form::textarea('description', '', ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Description Text'])}}
        </div>
        {{Form::submit('Speichern', ['class'=>'btn btn-sm btn-info front-btn'])}}
    {!! Form::close() !!}
@endsection
