@extends ( View::exists('layouts.blueboard')?'layouts.blueboard' : 'blueboard::layouts.blueboard')

@section ('content')
    <div class="row">
    <a href="/regkeys" class="btn btn-sm btn-default btn-info front-btn">Zurück</a>
    <div class="col-md-12 col-sm-12">
        <h1>{{$regkey->keyname}}</h1>
        <br><br>
        <strong>CODE: </strong>{!!$regkey->keycode!!}
    </div>
    <div class="col-md-12 col-sm-12 text-white" >
        <a href="/regkeys/{{$regkey->id}}/community_description" class="btn btn-sm btn-primary" style="color:white;" >Beschreibungstext der Gemeinschaft</a>
    </div>
    <hr />
        <div class="col-xs-12 top-buffer">
            {{Form::label('description', 'Interne Beschreibung:')}}
            {!!$regkey->description!!}
        </div>

    <hr>
    <small>Verfasst am {{$regkey->created_at}} von {{$regkey->creator->name}}</small>
    <hr>
    <a href="/regkeys/{{$regkey->id}}/edit" class="btn btn-sm btn-info front-btn">bearbeiten</a>
    </div>
@endsection
