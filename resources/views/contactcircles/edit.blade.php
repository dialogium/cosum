@extends('layouts.app')

@section('content')
    <h1>Kontaktkreis bearbeiten</h1>
    {!! Form::open(['action' => ['ContactCirclesController@update', $contactcircle->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
        <div class="form-group">
            {{Form::label('name', 'Name')}}
            @if ($isCommunityCircle)
                {{Form::text('name', $contactcircle->name, ['class' => 'form-control','readonly', 'placeholder' => 'Name'])}}
            @else
                {{Form::text('name', $contactcircle->name, ['class' => 'form-control', 'placeholder' => 'Name'])}}
            @endif
        </div>
        <div class="form-group">
        <a class="btn" onclick="var sf=document.getElementById('selectorForm'); sf.submit();">
            <span class="glyphicon glyphicon-plus"></span>Kontakt</a>
        @foreach ($contacts as $contact)
            <a class="tag label label-danger" href="/kontaktkreise/{{$contactcircle->id}}/remove/{{$contact->id}}">{{$contact->called}}
            <i class="remove glyphicon glyphicon-remove-sign glyphicon-white"></i>
            </a>
        @endforeach
        </div>

        {{Form::hidden('_method','PUT')}}
        {{Form::submit('Speichern', ['class'=>'btn btn-primary'])}}
    {!! Form::close() !!}
    
    {!! Form::open(['id' => 'selectorForm' ,'action' => ['ContactsController@select'], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
        {{Form::hidden('returnAddress', '/kontaktkreise/'.$contactcircle->id.'/add', ['hidden' => 'hidden','class' => 'form-control', 'placeholder' => 'url'])}}
    {!! Form::close() !!}
@endsection
