@extends('layouts.app')

@section('content')
    <a href="{{ URL::previous() }}" class="btn btn-default">Zurück</a>
    <h1>{{$contactcircle->name}}</h1>
    <hr>
    <small>Verfasst am {{$contactcircle->created_at}} by {{$contactcircle->user->name}}</small>
    <hr>
    @if(!Auth::guest())
        @if(Auth::user()->id == $contactcircle->user_id)
            <a href="kontaktkreise/{{$contactcircle->id}}/edit" class="btn btn-default">bearbeiten</a>

            {!!Form::open(['action' => ['ContactCirclesConroller@destroy', $contactcircle->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
                {{Form::hidden('_method', 'DELETE')}}
                {{Form::submit('löschen', ['class' => 'btn btn-danger'])}}
            {!!Form::close()!!}
        @endif
    @endif
@endsection
