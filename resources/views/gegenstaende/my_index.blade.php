@extends('layouts.app')

@section('content')
        @component ('search.comp')
        @endcomponent
         <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
               <a href="/gegenstaende/create" class="btn btn-sm btn-info">Gegenstand anlegen</a>
            </div>
            <div class="col-md-2"></div>
        </div>

    <h1>
        Meine Gegenstände
    </h1>

    <?php
        $categories=App\Gegenstand::$categories;
        $category_active_class="label-info";
    ?>

    @foreach ($categories as $category)
        <?php $cat= request()->cat; $cat_class=($category == $cat || ($cat == null  && $category == "alle" ))? $category_active_class : "label-default" ?>
        <label class="label {{$cat_class}}" for="" onclick="switchto('{{$category}}')" >{{$category}}</label>

    @endforeach

    <br />
    <br />

    <?php
        $std_btn_classes="btn btn-xs btn-default btn-secondary";
    ?>
    Filterung:
    <div class="btn-group btn-group-toggle" data-toggle="buttons">
    <?php
            $lent_active = (request()->has("lent"))?"active":"";
            $due_active = (request()->has("due"))?"active":"";
            $unlent_active = (request()->has("lent") || request()->has("due") )?"":"active";
    ?>
      <label class="{{ $std_btn_classes }} {{$lent_active}}" onclick="filter('lent');" name="lbl_radio_lent">
        <input type="radio" name="options" id="option1"> Verliehenes</input>
      </label>
      <label class="{{ $std_btn_classes }} {{$due_active}}" onclick="filter('due');" name="lbl_radio_due">
        <input type="radio" name="options" id="option2"> Fälliges</input>
      </label>
      <label class="{{ $std_btn_classes }} {{$unlent_active}}" onclick="filter('none');" name="lbl_radio_unlent">
        <input type="radio" name="options" id="option0"> Keine</input>
      </label>
    </div>
    Sortierung:
    <div class="btn-group btn-group-toggle" data-toggle="buttons">
     <?php
            $time_active = (request()->has("time"))?"active":"";
            $alpha_active = (request()->has("alpha"))?"active":"";
     ?>
      <label class="{{$std_btn_classes}} {{$alpha_active}}" onclick="sort('alpha');" name="lbl_radio_alpha">
        <input type="radio" name="options" id="option1" > Alphabetisch</input>
      </label>
      <label class="{{$std_btn_classes}} {{$time_active}}" onclick="sort('time');" name="lbl_radio_time">
        <input type="radio" name="options" id="option2"> Zeitlich</input>
      </label>
    </div>
    <div class="row top-buffer">
    @if(count($gegenstaende) > 0)
        {{$gegenstaende->appends(request()->except('page'))->links()}}
        <br/>
        @foreach($gegenstaende as $gegenstand)
                    @if ( $loop->iteration % 4 == 0 )
                        <div class="row">
                    @endif
                    <div class="well col-md-3 col-sm-3">
                        <a href="/gegenstaende/{{$gegenstand->id}}">

                            <img class="{{($gegenstand->lent)?'unavailable':'available'}}" style="width:100%" src="/storage/cover_images/{{$gegenstand->cover_image}}">
                           @if ($gegenstand->lent)
                                    @if ($gegenstand->isOverDue())
                                       <div class="ribbon-container ribbon-container-bg-due">
                                            <span href="#" class="ribbon">
                                                fällig
                                            </span>
                                       </div>
                                    @else
                                       <div class="ribbon-container ">
                                            <span href="#" class="ribbon">
                                                verliehen
                                            </span>
                                       </div>
                                    @endif
                           @endif
                        </a>
                            @if ($gegenstand->gift)
                                <span class="giftbanner btn-sm btn-default btn-warning active input-group-addon" >Zu verschenken</span>
                            @endif
                           @if ($gegenstand->category)
                            <span class="category_lbl btn-sm btn-default btn-info">
                                    <b><label>{{$gegenstand->category}}</label></b>
                            </span>
                            @endif
                        <h4 class="card_title"><a href="/gegenstaende/{{$gegenstand->id}}">{{$gegenstand->name}}</a></h4>
                        <strong><a href="/orte/{{$gegenstand->ort_id}}">{{$gegenstand->ort_name}}</a></strong><br/>
                        <small>Ersteintragung am {{$gegenstand->created_at}}</small>
                        @if ($gegenstand->lent)
                            <div class='input-group date top-buffer' >
                                <span class="input-group-addon">
                                    <label for="lent_to_date">Bis:</label>
                                </span>
                                <span class="input-group-addon">
                                    <label for="lent_to_date" class="{{$gegenstand->isOverDue()?'text-danger':''}}">{{$gegenstand->lent_to_date}}</label>
                                </span>

                            </div>
                        @endif
                        <br/>
                        <a href="/gegenstaende/{{$gegenstand->id}}/edit" class="btn btn-sm btn-default">bearbeiten</a>
                        <a class="btn btn-sm btn-default" href="/gegenstaende/{{$gegenstand->id}}/offenlegen">Gegenstand offenlegen</a>
<br/>
<br/>
                                        {!!Form::open(['action' => ['GegenstaendeController@destroy', $gegenstand->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
                                            {{Form::hidden('_method', 'DELETE')}}
                                            {{Form::submit('löschen', ['class' => 'btn btn-sm btn-danger'])}}
                                        {!!Form::close()!!}
                    </div>

                    @if ($loop->iteration % 4 == 0  ||  $loop->iteration == count($gegenstaende) )
                        </div >
                    @endif
        @endforeach
        {{$gegenstaende->appends(request()->except('page'))->links()}}
    @else
        <p>Keine Gegenstände gefunden.</p>
    @endif
    </div>
@endsection

@section ('page_script_code')
        (function() {
            searchOnMyItemsOnly();
        })();

        function filter(p){
            var params=new URLSearchParams(window.location.search);
                params.delete('lent');
                params.delete('due');

                if (p != "none"){
                    params.set(p,1);
                    //reset page to avoid jump to empty page
                    params.set('page',1);
                }

                window.location.search= params;
        }

        function switchto(new_cat){
            var params=new URLSearchParams(window.location.search);
                params.set('cat', new_cat);
                //reset page to avoid jump to empty page
                params.set('page',1);
            window.location.search= params;
        }

        function sort(kind){
            var params=new URLSearchParams(window.location.search);
            if (kind == "time"){
                params.set('time',1);
                params.delete('alpha');
            }
            if (kind == "alpha"){
                params.set('alpha',1);
                params.delete('time');
            }
            window.location.search= params;
        }

        /*inject searform with search on my gegenstände only*/
        function searchOnMyItemsOnly(){
            var sf=document.getElementById('searchform');
            var myitemsonly= document.createElement('input');
                myitemsonly.type="checkbox";
                myitemsonly.name="myitemsonly";
                myitemsonly.innerHTML="nur meine Gegenstände";
                myitemsonly.value="true";
                myitemsonly.setAttribute('checked','checked');
                myitemsonly.setAttribute('hidden','hidden');
            sf.appendChild(myitemsonly);
        }

@endsection
