@extends ( View::exists('layouts.blueboard')?'layouts.blueboard' : 'blueboard::layouts.blueboard')

@section ('content')
    <a href="/blueboard" class="btn btn-sm btn-default btn-info front-btn">Übersicht</a>
    <a href="/proposals/create" class="btn btn-sm btn-default btn-success front-btn">Text anlegen</a>
    <h1>Vereinbarungstexte</h1>
    @if (count($proposals) > 0)
        @foreach ($proposals as $proposal)
            <div class="jumbotron">
                <div class="row">
                    <div class="col-md-8 col-sm-8">
                        <h3><a href="/proposals/{{$proposal->id}}">{{$proposal->title}}</a></h3>
                        <small>Verfasst am {{$proposal->created_at}} von {{$proposal->user->name}}</small>
                    </div>
                </div>
            </div>
        @endforeach
        {{$proposals->links()}}
    @else
        <p>Kein Eintrag gefunden</p>
    @endif
@endsection
