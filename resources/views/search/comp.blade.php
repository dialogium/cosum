            <div class="row top-buffer">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                {!! Form::open(['url' => '/search', 'id' => 'searchform', 'class'=>'form searchform', 'style' => 'margin-bottom:15px;margin-top:-23px;']) !!}
                    <div class="input-group top-buffer">
                    @php
                        $old=(isset($search))?$search:"";
                    @endphp
                    {!! Form::text('search', $old , ['required', 'class'=>'form-control input-group-text searchinput', 'placeholder'=>'Wonach suchst du?']) !!}
                    <span class="input-group-addon" onclick='var sf=document.getElementById("searchform");sf.submit();' style="cursor:pointer;">
                        <span  class="glyphicon glyphicon-search" aria-hidden="true"></span>
                    </span>
                    </div>
                    @if (isset($myitemsonly) && $myitemsonly != "")
                        <input type="checkbox" name="myitemsonly" id="myitemsonly" value="true" checked > <span>nur meine Gegenstände</span> </input>
                    @endif
                {!! Form::close() !!}
                </div>
                <div class="col-md-2"></div>
            </div>
