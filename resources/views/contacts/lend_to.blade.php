@extends('layouts.app')

@section('content')
<div class="col-md-6">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Liste der Kontakte</h3>
        </div>
        <div class="panel-body">
     @if (count($contacts) > 0)
        @foreach ($contacts as $contact)
                <div class="col-md-6 col-sm-6">
                    @if ($corr_token)
                        <h4>
                            <a href="/kontakte/{{$contact->id}}/edit?corr_token={{$corr_token}}">{{$contact->called}}</a>
                        </h4>
                    @elseif ($obj_id && !$lend)
                        <h4>
                        @if ($contact->status == 'made')
                            <a href="/offenlegungen/create?obj_id={{$obj_id}}&rcpt_id={{$contact->id}}">{{$contact->called}} </a>
                        @else
                            <label>{{$contact->called}} </label>
                        @endif
                        </h4>
                    @elseif ($lend)
                        <h4>
                            <a href="/gegenstaende/{{$lend}}/edit?lent_to={{$contact->id}}">{{$contact->called}}</a>
                        </h4>

                    @else
                        <h4>
                            <a href="/kontakte/{{$contact->id}}/edit">{{$contact->called}}</a>
                        </h4>
                    @endif
                        @if ($contact->status == 'made')
                                <span class="glyphicon glyphicon-link">
                                    <small> <strong>@lang('contact.status.'.$contact->status)</strong> </small>
                                </span>
                        @endif
                        @if ($contact->deleted_user)
                            (gelöschter Benutzer)
                        @endif
                        <br />
                        <small>Angelegt am {{$contact->created_at}}</small>
                </div>
        @endforeach
    @else
        <p>Keine Bekanntschaften</p>
    @endif
        </div>
    </div>
</div>
@endsection
