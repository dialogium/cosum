@extends('layouts.app')

@section('content')

@section ('branding_area')
                @include('inc.branding_ort',['brandingColor' => $ort->color, 'brandingName' => $ort->name ] )
@endsection
    <a href="{{ URL::previous() }}" class="btn btn-default">Zurück</a><br>

    @if ($ort->address)
        <div class='input-group top-buffer' >
            <strong>Adresse:</strong><br><span class="">
		    <span class="glyphicon glyphicon-pushpin"></span>
		    </span>
                    <span>
                    <a class="input" href="https://nominatim.openstreetmap.org/search?q={{  urlencode($ort->address) }}">Auf Karte anzeigen</a>
                    </span>
        </div>
    @endif
    <div class="top-buffer">
        {!!$ort->beschreibung!!}
    </div>
    <hr>
    <small>Verfasst am {{$ort->created_at}} </small>
    <hr>
    @if(!Auth::guest())
        @if(Auth::user()->id == $ort->user_id)
            <a href="/orte/{{$ort->id}}/edit" class="btn btn-default">bearbeiten</a>

            {!!Form::open(['action' => ['OrteController@destroy', $ort->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
                {{Form::hidden('_method', 'DELETE')}}
                {{Form::submit('löschen', ['class' => 'btn btn-danger'])}}
            {!!Form::close()!!}
        @endif
    @endif
@endsection
