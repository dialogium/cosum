<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/body_pattern.css') }}" rel="stylesheet" title="">
    <link href="{{ asset('css/green_on_white.css') }}" rel="stylesheet" title="green on white">
    <link href="{{ asset('css/black_on_green.css') }}" rel="alternate stylesheet" title="black on green">
    <link href="{{ asset('css/post_styles.css') }}" rel="stylesheet">
<style type="text/css" >
    .errcode{
        font-family: 'Open Sans';
        font-size: 10em;
        /*color: #FF7A00;*/
        color: #FF7A00;
        text-align: center;
        margin-bottom: 1px;
        text-shadow: 4px 4px 1px gray; 
    } 
</style>
</head>
<body>
    <div id="app">
        @include('inc.navbar')
        
        <div class="container">
            <div class="row">
                
                    <div class="col">
                        <h1 class="errcode">
                            @yield('code', __('Oh no'))
                        </h1>
                    </div>

                    <div class="col-12"style="text-align:center">

                    <p class="center" >
                        @yield('message')
                    </p>

                    <a href="{{ url('/') }}">
                        <button class="bg-transparent text-grey-darkest font-bold uppercase tracking-wide py-3 px-6 border-2 border-grey-light hover:border-grey rounded-lg">
                            {{ __('home') }}
                        </button>
                    </a>
                {{-- @yield('image') --}}
                    </div>
            </div>
                
        </div>
	@include('inc.footer')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
