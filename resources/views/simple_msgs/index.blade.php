@extends ('layouts.app')

@section ('content')
    <h1>Nachrichten</h1>
    <hr />
    <h2>Empfangene Anfragen</h2>
    @if (count($smsgs) > 0)
        @foreach ($smsgs as $smsg)
            <div class="gesuchotron">
                <div class="row">
                    <div class="col-md-10 col-sm-0">
                        <h3><a href="/anfrage/{{$smsg->id}}">
                           @if (!$smsg->read)
                                <span class="glyphicon glyphicon-bell"></span>
                           @else
                                <span class="glyphicon glyphicon-list-alt"></span>
                           @endif
                            Anfrage
                            </a>
                                @if ($smsg->signal == null)
                                    <span class="glyphicon glyphicon-question-sign"></span>
                                @endif
                                @if ($smsg->signal == 1)
                                    <span class="glyphicon glyphicon-ok-circle"></span>
                                @endif
                                @if ($smsg->signal == 2)
                                    <span class="glyphicon glyphicon-remove-sign"></span>
                                @endif
                        </h3>
                    </div>
                    <div class="col-md-2 col-sm-2">
                       Datum: {{$smsg->created_at->format('d.m.Y')}}
                    </div>

                </div>
            </div>
        @endforeach
        {{$smsgs->links()}}
    @else
        <p>Derzeit keine Nachrichten</p>
    @endif
    <hr />
    <h1>Gesendete Anfragen</h1>
    @if (count($sent_smsgs) > 0)
        @foreach ($sent_smsgs as $smsg)
            <div class="gesuchotron">
                <div class="row">
                    <div class="col-md-9 col-sm-0">
                        <h3><a href="/anfrage/{{$smsg->id}}">
                            Anfrage
                            </a>
                                @if ($smsg->signal == null)
                                    <span class="glyphicon glyphicon-question-sign"></span>
                                @endif
                                @if ($smsg->signal == 1)
                                    <span class="glyphicon glyphicon-ok-circle"></span>
                                @endif
                        </h3>
                    </div>
                    <div class="col-md-2 col-sm-2">
                       Datum: {{$smsg->created_at->format('d.m.yy')}}
                    </div>
                    <div class="col-md-1 col-sm-1">
                        {!!Form::open(['action' => ['SimpleMsgController@destroy', $smsg->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
                            {{Form::hidden('_method', 'DELETE')}}
                            {{Form::submit('löschen', ['class' => 'btn btn-danger'])}}
                        {!!Form::close()!!}
                    </div>
                </div>
            </div>
        @endforeach
        {{$sent_smsgs->links()}}
    @else
        <p>Derzeit keine Nachrichten</p>
    @endif
@endsection
