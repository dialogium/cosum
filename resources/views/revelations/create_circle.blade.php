@extends('layouts.app')

@section('content')
    <h1>Neue Offenlegung</h1>
    {!! Form::open(['action' => 'RevelationController@store_from_circle', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
        <div class="col-lg-3 col-md-3 col-sm-3">
            {{Form::label('obj_name', 'Gegenstand:')}}
            <br />
            {{Form::label('obj_name', $gegenstand->name)}}
            <img style="width:100%" src="/storage/cover_images/{{$gegenstand->cover_image}}">
        </div>
        <div class="form-group top-buffer" hidden="hidden">
            {{Form::text('obj_id', $gegenstand->id, ['class' => 'form-control','id' => 'obj_id']) }}
        </div>
        <div class="form-group">
            {{Form::label('circle_name', 'Kreis:')}}
            <br />
            {{Form::label('circle_name', $contactcircle->name, [ "class" => "label label-success"])}}
        </div>
        <div class="form-group top-buffer" hidden="hidden">
            {{Form::text('circle_id', $contactcircle->id, ['class' => 'form-control', 'placeholder' => 'Id'])}}
        </div>

        {{Form::submit('Speichern', ['class'=>'btn btn-primary'])}}
    {!! Form::close() !!}
@endsection
