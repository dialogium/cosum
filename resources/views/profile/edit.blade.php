@extends('layouts.app')

@section('content')
        <h2>Meine Profil-Einstellungen</h2>
        <div class="row">
            <div class="col-sm-6">
            <h4><u>Zugangsdaten</u></h4>
            Benutzername:
                <strong>
                    {{ auth()->user()->name }}
                </strong>
            <br />
            Registrierte Email-Adresse:
                <strong>
                    {{ auth()->user()->email }}
                </strong>
                {!! Form::open(['action' => ['ProfileController@email_set'],  'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

                            <div class="">
                                <input id="old_email" type="hidden" class="form-control" name="old_email" value="{{ auth()->user()->email }}" hidden="hidden" required>
                                <label for="email" class=" control-label">Neue E-Mail-Adresse</label>
                                <input id="email" type="email" class="form-control" name="email" value="" required>
                                <label for="email_confirmation" class=" control-label">Bestätigen</label>
                                <input id="email_confirmation" type="email" class="form-control" name="email_confirmation" value="" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                <p>Durch den Klick auf "ändern" wird der <strong>Login blockiert </strong>und die neue Email-Adresse muss verifiziert werden. Die alte <strong>Email-Adresse wird gelöscht</strong>.</p>
                {{Form::submit('ändern', ['class'=>'btn btn-primary'])}}
            {!! Form::close() !!}
            </div>
            <div class="col-sm-6">
                {!! Form::open(['action' => ['ProfileController@update'],  'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                <div class="form-group">
                <h4><u>Email-Notifikationen</u></h4>
                   @if (auth()->user()->profile->email_notifications_on)
                      <input type="checkbox" name="email_notifications_on" checked >
                        <span class="glyphicon glyphicon-envelope"></span>
                        <span> Bitte bestätige, ob du Emails von Cosum erhalten möchtest? Beispielsweise Leihanfragen.</span>
                    @else
                      <input type="checkbox" name="email_notifications_on" >
                        <span class="glyphicon glyphicon-envelope"></span>
                        <span> Bitte bestätige, ob du Emails von Cosum erhalten möchtest. Beispielsweise Leihanfragen.</span>
                    @endif
                    <br />
                </div>
                <div class="form-group">
                    <h4><u>Wer bin ich?</u></h4>
                    Hinterlasse gern einen Beschreibungstext.
                    {{Form::textarea('Beschreibungstext', $profile->description, ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Beschreibungstext'])}}


                </div>
                {{Form::submit('speichern', ['class'=>'btn btn-primary'])}}
                {!! Form::close() !!}
            </div>

	</div>
        <hr />
        <div class="row">
            <div class="col-sm-6">
                <p>Mein Profil in Zahlen: </p>
            <a href="/profil/produce_log" class="btn btn-info">Übersicht</a>
            </div>
            <div class="col-sm-6">
                <p>Das Konto bei Cosum lässt sich löschen. Links befindet sich eine Übersicht, welche Daten davon betroffen wären. Natürlich kannst du auch vorher deine Gegenstände, Offenlegungen, Kontakte durchgehen und schauen, ob du nicht doch Cosumer bleiben möchtest.</p>
            <a href="/profil/loeschen" class="btn btn-danger">Konto löschen</a>
            </div>

        </div>
@endsection
