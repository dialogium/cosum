@extends('layouts.app')

@section('content')
        <h2>Konto löschen</h2>
        <div class="row">
            <div class="col-sm-6">
            <p>
Soll dieses Cosum-Konto wirklich gelöscht werden?
            Nach der Bestätigung werden alle Daten unzugänglich und der Login unmöglich bis die Daten vollständig gelöscht werden.
</p>
<br />
                {!! Form::open(['action' => ['ProfileController@softdelete'],  'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                {{Form::hidden('_method', 'DELETE')}}
                {{Form::submit('Konto wirklich löschen.', ['class'=>'btn btn-danger'])}}
                {!! Form::close() !!}  <br />
                <a href="{{ url()->previous() }}" class="btn btn-default">abbrechen<a>
            </div>
        </div>

@endsection
