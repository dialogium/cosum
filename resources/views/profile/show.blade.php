@extends('layouts.app')

@section('content')
        <h2>Meine Profil-Einstellungen</h2>
        <div class="row">
            <div class="col-sm-6">
            Benutzername:
                <strong>
                    {{ auth()->user()->name }}
                </strong>
            <br />
            Registrierte Email-Adresse:
                <strong>
                    {{ auth()->user()->email }}
                </strong>
            <br />
            <strong>Beschreibungstext:</strong><br />
            <div class="well">
                {!! auth()->user()->profile->description !!}
            </div>
            @if (auth()->user()->profile->email_notifications_on)
               <p><span class="glyphicon glyphicon-ok"></span> Du hast bestätigt, dass du von Cosum Email-Notifikationen bekommen möchstest.</p> 
            @else
                <p><span class="glyphicon glyphicon-remove"></span> Du hast dem Empfang von Email-Notifikationen nicht zugesitmmt.</p>
            @endif
                <br />
                <br />
                <a class="btn btn-warning" href="/profil/edit">
                    Bearbeiten
                </a>
            </div>
            <div class="col-sm-12">
            <hr />
                <p>Vielen Dank für das Verleihen von Gegenständen. <br />
Dadurch ergibt sich eine persönliche geschätzte Gesamt-Einsparung von <strong>{{ $total_savings }} Gramm CO2e</strong>.</p>
                </p>
            </div>
	</div>
@endsection
