@extends('layouts.app')

@section('content')
<div class="row">
            <h1>Gemeinschaften</h1>
            {!! Form::open(['action' => ['ProfileController@enter_community'],  'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
            <div class="col-md-8">
<!--                <div class="panel-heading">Bilde Gemeinschaften mit realen Menschen!</div> -->
                <div class="panel-body reddish_text">
                <p>
Du möchtest deine Gemeinschaft (z.B. Nachbarschaft oder Freundeskreis) zum Leihen auf Cosum abbilden? Sende uns dafür eine Mail an <!-- <a href="mailto:gerard@cosum.de?Subject=Schlüssel" target="_top">gerard(at)cosum.de</a> oder --> <a href="mailto:nikolai@cosum.de?Subject=Schlüssel" target="_top">nikolai(at)cosum.de</a> mit kurzer Beschreibung eurer Gemeinschaft. Dann erhältst du von uns einen Schlüssel mit dem Namen der Gemeinschaft & einem Code. <strong>Teile diesen allen mit</strong>, die zur Gemeinschaft gehören.
<br/><br />
Mit der Funktion <strong>'Auto-Verbindung'</strong> werden alle Kontakte der Gemeinschaft automatisch verbunden.
<br/>
Mit der Funktion <strong>'Gegenstand offenlegen'</strong> könnt ihr euch Gegenstände untereinander exclusiv sichtbar machen. Diese Funktion findest du unter "Meine Gegenstände" <strong>-></strong>  "bearbeiten" <strong>-></strong> "Gegenstand offenlegen".
<br/>
                </p>
                <p>
<h4>Leihen, wie du willst.</h4>

Du kannst deine Gegenstände einzelnen Personen, Kontaktkreisen oder Gemeinschaften sichtbar machen. Dazu gehst du auf "Meine Gegenstände" <strong>-></strong> "bearbeiten" und klickst auf "Gegenstand offenlegen".
<br /><br />
Viel Freude!

                </p>
                </div>
            </div>

            <div class="col-md-8">
            <div class="panel panel-default panel-warning">
                <div class="panel-heading" style="font-size:larger;"><strong>Schlüssel um einer Gemeinschaft beizutreten</strong></div>

                <div class="panel-body">
                        <p>
                        Der Schlüssel besteht aus dem Namen der Gemeinschaft und einem Code.
                        </p>
                        <div class="form-group{{ $errors->has('keyname') ? ' has-error' : '' }}">
                            <label for="keyname" class="col-md-4 control-label">Name der Gemeinschaft</label>
                            <div class="col-md-6">
                                <input id="keyname" type="text" class="form-control" name="keyname" value="{{ old('keyname') }}" required>

                                @if ($errors->has('keyname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('keyname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('keycode') ? ' has-error' : '' }}">
                            <label for="keycode" class="col-md-4 control-label">Code</label>

                            <div class="col-md-6">
                                <input id="keycode" type="text" class="form-control" name="keycode" value="{{ old('keycode') }}" required>

                                @if ($errors->has('keycode'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('keycode') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="auto_connect" class="col-md-4 control-label">Auto-Verbindung</label>
                            <div class="col-md-6">
                                {{Form::hidden('auto_connect',auth()->user()->auto_connect)}}
                                {{Form::checkbox('auto_connect')}}
                            <p>Ich möchte mit Gemeinschaftsmitgliedern automatisch verbunden werden. <br/> Der Benutzername wird bei einer Verbindung einmalig bekannt gegeben.</p>
                            </div>
                        </div>

                </div>
                {{Form::submit('Beitreten', ['class'=>'btn btn-primary'])}}
                </div>
        </div>
                {!! Form::close() !!}
</div>

<div class="row top-buffer-big">
            <div class="col-md-8">
                <div class="panel panel-default panel-info">
                    <div class="panel-heading" style="font-size:larger;"><strong>Gemeinschaften, in denen du Mitglied bist</strong></div>

                    <div class="panel-body">
                        <ul>
                        @foreach ($communities as $community)
                            <li>
                            <a id="" href="/regkeys/{{$community->id}}/community_description"><strong>{{$community->keyname}}</strong></a>
                            </li>
                        @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
</div>

@endsection
