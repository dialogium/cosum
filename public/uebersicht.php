<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="GtQvhtf46EjoJcCAZJQgzVM6AKjDA3tPwim0lX9E">
    <title>Cosum.de</title>
    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
    <link href="/css/body_pattern.css" rel="stylesheet" title="">
    <link href="/css/green_on_white.css" rel="stylesheet" title="green on white">
    <link href="/css/black_on_green.css" rel="alternate stylesheet" title="black on green">
    <link href="/css/post_styles.css" rel="stylesheet">
    <link href="/css/redesign.css" rel="stylesheet">
</head>
<body class="cosum_bg_silver">
<?php 

    $cosum_instances = [
            0 => ['name' => 'Berlin', 'url' => 'https://berlin.cosum.de', 'descr' => 'Initiale Plattform von Cosum.de .','status' => '' ],
            1 => ['name' => 'Stadt Brandenburg','url' => 'https://brb.cosum.de', 'descr' => 'Zweite Plattform-Instanz' ,'status' => '' ],
            2 => ['name' => 'Rheinland Pfalz','url' => 'https://rlp.cosum.de', 'descr' => 'Dritte Plattform-Instanz' ,'status' => '' ],
            /* 2 => ['name' => '...','url' => '#', 'descr' => 'Weitere Zweige in der Planung' ,'status' => 'info' ]*/
            3 => ['name' => 'NRW','url' => 'https://nrw.cosum.de', 'descr' => 'Freigegeben!' ,'status' => '' ],
            4 => ['name' => 'Bayreuth (unabhängig)','url' => 'https://leila.transition-bayreuth.de/', 'descr' => 'Unabhängiger Zweig in Bayreuth' ,'status' => 'building' ],
    ];

?>
<div id="app">
    <nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <!-- Branding Image -->
            <a class="brandname navbar-brand" href="#">
                <img class="cosum_logo_in_blue" src="/img/cosum_logo_and_claim_desat.png" width="200px"/>
            </a>
        </div>
    </div>
    </nav>
    <div class="container">
        <div class="col-md-12">
            <div class="jumbotron comic_item_back text-center">
                    <p>
                    <span ><img style="width:100%" src="/img/cosum_logo_and_claim_desat.png" /> </span>
                    </p>
                    <p style="color:white;font-weight:bold;">Die Plattform zum nachhaltigen Leihen und Schenken</p>
            </div>
        </div>

        <div class="col-md-12">
            <div class="jumbotron text-center cosum_text_blue cosum_bg_silver">
               <?php foreach ($cosum_instances as $key => $instance){ ?>
               <a   class="btn front-btn"
                    href="<?php echo $instance['url'] ?>"
                    title="<?php echo $instance['descr']; ?>"
                >
                <?php if ($instance['status'] == 'building'){ ?>
                    <i class="bi bi-exclamation-triangle"></i>
                <?php } ?>

                     <b><?php echo $instance['name'] ?></b>
               </a> <br />
               <?php } ?>
            </div>
        </div>
        <div class="col-md-12">
             <div class="jumbotron text-center cosum_text_blue cosum_bg_silver">
                 <p>
                 <strong>Ihr möchtet in eurer Region mehr Nachhaltigkeit?</strong><br/>
                 Wir können für euch eine neue Instanz von Cosum eröffnen.<br/>
                 Meldet euch einfach bei
                 <a class="" href="mailto:support@cosum.de?Subject=[Cosum] Instanz-Hosting">support(at)cosum.de</a>.<br />
                 Wir freuen uns auf euch!
                </p>
             </div>
        </div>

    </div>

        <div class="footer cosum_bg_blue">
        <div class="container">
            <div class="row">
                <div class="col-md-3">

                <h1>Kontakt</h1>
                <p>
                <a class="btn btn-xs btn-info front-btn top-buffer" href="mailto:nikolai@cosum.de?Subject=[Cosum] Kontakt" target="_top">nikolai(at)cosum.de</a><br />
                <!-- <a class="btn btn-xs btn-info front-btn top-buffer" href="mailto:gerard@cosum.de?Subject=[Cosum] Kontakt" target="_top">gerard(at)cosum.de</a> <br /> -->
                <a class="btn btn-xs btn-success front-btn top-buffer" href="http://cosum-blog.de">Zum Blog</a>
                </p>

                <a class="btn btn-lg btn-info front-btn top-buffer" href="https://berlin.cosum.de/impressum" target="_top">Impressum</a>
                <a class="btn btn-lg btn-info front-btn top-buffer" href="https://gitlab.com/dialogium/cosum" target="_top">OpenSource!</a>
                <div class="alert alert-info"><strong>Datenschutzerklärung:</strong> <br /><a href="/dokumente/datenschutz_v2.pdf">datenschutz_v2.pdf</a></div>
                </div>
                <div class="col-md-1">
                </div>
                <div class="col-md-4">
                    <h2>Empfohlen von</h2>
                    <a href="https://www.bund-berlin.de/">
                        <img style="width:50%" src="/img/bund_weiss_transp.png">
                    </a>
                    <h2>Teil von</h2>
                    <!-- <img style="width:50%" src="/img/reuse-berlin.png"> -->
                    <a href="http://www.berlin.de/re-use" class="">
                        <img style="width:50%" src="/img/re-use-logo_2.png">
                    </a>
                    <a href="http://zero-waste-berlin.de/" class="">
                        <img style="width:50%" src="/img/zero_waste.png">
                    </a>
                </div>

                <div class="col-md-4 col-lg-4">
                    <div class=".top-buffer"></div>
                    <h2>Gefördert durch</h2>
                    <!-- <a href="https://www.berlin.de/sen/uvk/" class="supporter"> -->
                    <a href="https://www.berlin.de/sen/uvk/umwelt/kreislaufwirtschaft/projekte/re-use-berlin/re-use-zentrum/" class="supporter">
                        <img style="width:100%" src="/img/bsen_uvk_logo_2021.jpg">
                    </a>
                    <p class="">
                        Gefördert im Rahmen des Berliner Energie- und Klimaschutzprogramms 2030
                    </p>
                    <a href="" class="supporter">
                        <img style="width:100%" src="/img/koop5_logoleiste.jpg">
                    </a>

                    <div class=".top-buffer"></div>
                    <a href="https://www.klimaschutz.de/" class="supporter">
                        <img style="width:100%;" src="/img/bmub_nki.jpg">
                    </a>

                    <a href="https://www.stiftung-naturschutz.de/startseite/" class="supporter">
                        <img style="width:50%" src="/img/stiftung_naturschutz.jpg">
                    </a>


                </div>
                <p>&nbsp;</p>
            </div>
        </div> <!-- container --!>
        </div> <!-- footer  --!>
</div> <!-- app  --!>
</body>
</html>
