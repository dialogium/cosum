<?php

namespace Dialogium\Blueboard\Tests;

use Dialogium\Blueboard\Facades\Blueboard;
use Dialogium\Blueboard\ServiceProvider;
use Orchestra\Testbench\TestCase;

class BlueboardTest extends TestCase
{
    protected function getPackageProviders($app)
    {
        return [ServiceProvider::class];
    }

    protected function getPackageAliases($app)
    {
        return [
            'blueboard' => Blueboard::class,
        ];
    }

    public function testExample()
    {
        $this->assertEquals(1, 1);
    }
}
