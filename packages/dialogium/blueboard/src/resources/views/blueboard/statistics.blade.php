@extends ( View::exists('layouts.blueboard')?'layouts.blueboard' : 'blueboard::layouts.blueboard')

@section ('content')
              <h1>Statistiken</h1>
              <div class="row alert alert-warning">
                <div class="col-sm-4 col-md-4 col-lg-4">
                    <a href="/blueboard" class="btn btn-sm btn-warning front-btn">Zurück</a>
                </div>
              </div>
              <div class="row jumbotron">
                    <h2>Benutzer</h2>
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <p>Betreiber: {{ $operator_count }}</p>
                    </div>
                    <div class="col-sm-4 col-md-4 col-lg-4">
                        <p>Email-Notifikation An: {{ $users_with_email_notifications_on_count }}</p>
                    </div>
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <p>Gesamt: {{ $user_count }}</p>
                    </div>
              </div>
              <div class="row jumbotron">
                    <h2>Gegenstände</h2>
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <p>Öffentliche: {{ $gegenstaende_pub_count }}</p>
                    </div>
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <p>Kommunale: {{ $gegenstaende_communal_count }}</p>
                    </div>
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <p>Private: {{ $gegenstaende_private_count }}</p>
                    </div>
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <p>Gesamt: {{ $gegenstaende_count }}</p>
                    </div>
               </div>
                <div class="row jumbotron">
                    <h2>Leihvorgänge</h2>
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <p>Offene: {{ $open_lending_count }}</p>
                    </div>
                    <div class="col-sm-4 col-md-4 col-lg-4">
                        <p>Abgeschlossene: {{ $finished_lending_count }}</p>
                        <br />
                        {{ $co2->days }} Leih-Tage x {{ CO2::CO2_FACTOR }} g CO2e/Leih-Tage = <br /> <strong> {{ $co2->total_savings }} Gramm CO2e</strong>
                    </div>
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <p>Gesamt: {{ $lending_count }}</p>
                    </div>
               </div>

@endsection
