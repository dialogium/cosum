@extends ( View::exists('layouts.blueboard')?'layouts.blueboard' : 'blueboard::layouts.blueboard')

@section ('content')
              <h1>Benutzer-Verwaltung</h1>
              <div class="row alert alert-warning">
                <div class="col-sm-4 col-md-4 col-lg-4">
                    <a href="/blueboard" class="btn btn-sm btn-warning front-btn">Zurück</a>
                </div>
              </div>

              <div class="row jumbotron">
                    <h2>Benutzer</h2>
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <p>{{ $num_users }}</p>
                    </div>
              </div>
              @foreach ( $users as $user )
                    <div class="row jumbotron {{ ($user->deleted_at)? 'trashed' :'' }}">
                        <div class="col-md-1 col-sm-1">
                            @if ($user->is_valid())
                                <i class="glyphicon glyphicon-ok-sign" title="gültiger" ></i>
                            @endif
                            @if (!$user->is_valid())
                                <i class="glyphicon glyphicon-remove-sign" title="ungültiger Benutzer"></i>
                            @endif
                            @if ($user->deleted_at)
                                <br />
                                <i class="glyphicon glyphicon-trash" title="Löschung vorgemerkt"></i>
                            @endif
                        </div>
                        <div class="col-md-5 col-sm-5">
                        {{$user->email}}
                            @if ($user->deleted_at)
                                <br />
                               {{ Carbon\Carbon::parse($user->deleted_at)->format('d.m.Y')}}
                            @endif
                        </div>
                        <div class="col-md-2 col-sm-2">
                        <i title="id">{{$user->id}}</i>&nbsp;
                            @if ($user->isOperator())
                                <i class="glyphicon glyphicon-wrench" title="Operator"></i>
                            @endif
                            @if ($user->isBanned())
                                <i class="glyphicon glyphicon-alert" title="verbannt"></i>
                            @endif
                        </div>

                        <div class="col-md-1 col-sm-1">
                            <a href="/blueboard/user_manage/{{$user->id}}" title="bearbeiten"  name="btn-manage" id="" class="glyphicon glyphicon-pencil"></a>
                        </div>
                    </div>
                    @endforeach
@endsection
