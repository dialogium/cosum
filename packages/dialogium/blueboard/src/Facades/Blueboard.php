<?php

namespace Dialogium\Blueboard\Facades;

use Illuminate\Support\Facades\Facade;

class Blueboard extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'blueboard';
    }
}
