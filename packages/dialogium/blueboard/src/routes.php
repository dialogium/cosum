<?php
Route::middleware(['web'])->group(function () {
    Route::get('blueboard/', 'Dialogium\Blueboard\Http\Controllers\BlueBoardController@blueboard');
    Route::get('blueboard/statistics', 'Dialogium\Blueboard\Http\Controllers\BlueBoardController@statistics');
    Route::get('blueboard/user_management', 'Dialogium\Blueboard\Http\Controllers\UserManagementController@index');
    Route::get('blueboard/item_management', 'Dialogium\Blueboard\Http\Controllers\ItemManagementController@items');
    Route::get('blueboard/user_manage/{id}', 'Dialogium\Blueboard\Http\Controllers\UserManagementController@manage');
    Route::get('blueboard/item_manage/{id}', 'Dialogium\Blueboard\Http\Controllers\ItemManagementController@manage');
    Route::post('blueboard/user_ban_post', 'Dialogium\Blueboard\Http\Controllers\UserManagementController@ban_post');
    Route::post('blueboard/user_unban_post', 'Dialogium\Blueboard\Http\Controllers\UserManagementController@unban_post');
    Route::post('blueboard/user_validate_post', 'Dialogium\Blueboard\Http\Controllers\UserManagementController@validate_post');
    Route::post('blueboard/user_invalidate_post', 'Dialogium\Blueboard\Http\Controllers\UserManagementController@invalidate_post');
    Route::post('blueboard/user_undelete_post', 'Dialogium\Blueboard\Http\Controllers\UserManagementController@undelete_post');
    Route::post('blueboard/item_block_post', 'Dialogium\Blueboard\Http\Controllers\ItemManagementController@block_post');
    Route::post('blueboard/item_unblock_post', 'Dialogium\Blueboard\Http\Controllers\ItemManagementController@unblock_post');
    Route::get('blueboard/users/banned', 'Dialogium\Blueboard\Http\Controllers\UserManagementController@banned_users');
});
