<?php

namespace Dialogium\Blueboard;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    const CONFIG_PATH = __DIR__ . '/../config/blueboard.php';

    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/routes.php');
        $this->loadViewsFrom(__DIR__.'/resources/views/blueboard/','blueboard');
        $this->publishes([
            self::CONFIG_PATH => config_path('blueboard.php'),
        ], 'config');
    }

    public function register()
    {
        $this->mergeConfigFrom(
            self::CONFIG_PATH,
            'blueboard'
        );

        $this->app->bind('blueboard', function () {
            return new Blueboard();
        });
    }
}
