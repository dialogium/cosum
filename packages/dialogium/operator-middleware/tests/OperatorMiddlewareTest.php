<?php

namespace Dialogium\OperatorMiddleware\Tests;

use Dialogium\OperatorMiddleware\Facades\OperatorMiddleware;
use Dialogium\OperatorMiddleware\ServiceProvider;
use Orchestra\Testbench\TestCase;

class OperatorMiddlewareTest extends TestCase
{
    protected function getPackageProviders($app)
    {
        return [ServiceProvider::class];
    }

    protected function getPackageAliases($app)
    {
        return [
            'operator-middleware' => OperatorMiddleware::class,
        ];
    }

    public function testExample()
    {
        $this->assertEquals(1, 1);
    }
}
