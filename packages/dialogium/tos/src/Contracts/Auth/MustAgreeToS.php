<?php

namespace Dialogium\tos\Contracts\Auth;

interface MustAgreeToS
{
    public function hasToS();
    public function setToS($id);
    public function tos();
}
