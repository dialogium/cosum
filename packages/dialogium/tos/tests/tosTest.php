<?php

namespace Dialogium\tos\Tests;

use Dialogium\tos\Facades\tos;
use Dialogium\tos\ServiceProvider;
use Orchestra\Testbench\TestCase;

class tosTest extends TestCase
{
    protected function getPackageProviders($app)
    {
        return [ServiceProvider::class];
    }

    protected function getPackageAliases($app)
    {
        return [
            'tos' => tos::class,
        ];
    }

    public function testExample()
    {
        $this->assertEquals(1, 1);
    }
}
